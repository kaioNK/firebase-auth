const signInGoogle = () => {
   console.log("signInGoogle called...")
   const provider = new firebase.auth.GoogleAuthProvider()
   provider.addScope("https://www.googleapis.com/auth/contacts.readonly")

   firebase .auth().signInWithPopup(provider)
      .then((result) => {
         const user = result.user 
         console.log("User ===== ", user)
      })
      .catch((error) => {
         console.error("Erro ao se autenticar com o Google.Erro: " + error.message)
      })
}
