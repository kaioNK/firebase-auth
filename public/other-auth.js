/*Logando apenas com email. Devemos habilitar isso no console do Firebase*/
//Não tá funcionando. Envia o email, mas mostra como se o usuário não estivesse logado.
const sendLink = (emailAddress) => {
   console.log("emailLogin called!")

   const actionCodeSettings = {
      url: 'http://localhost:5000',
      handleCodeInApp: true
   }

   firebase.auth().sendSignInLinkToEmail(emailAddress, actionCodeSettings)
      .then((success) => {
         window.localStorage.setItem('emailForSignIn', emailAddress)
         console.log("Método: sendLink => Email enviado com sucesso!")
      })
      .catch((error) =>{
         console.error("Método: sendLink => Erro ao enviar email para login. Erro: " + error.message)
      })
}

const anonymousLogin = () => {
   console.log("anonymousLogin called!")
   firebase.auth().signInAnonymously()
      .then(() => {
         console.log("Método: anonymousLogin => OK!")
      })
      .catch((error) => {
         console.error("Método: anonymousLogin => Erro: " + error.message)
      })
}

const numberLogin = (phoneNumber) => {
   console.log("numberLogin called!")
   const appVerifier = new firebase.auth.RecaptchaVerifier("recaptcha-security")

   firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
      .then((confirmationResult) => {
         //confirmando o código enviado
         const code = window.prompt('Please, enter the 6 digit code!')
         return confirmationResult.confirm(code)
      })
      .then((result) => {
         document.getElementById('recaptcha-security').innerHTML = "Authenticaded!"
      })
      .catch((error) => {
         console.error("Método: numberLogin ==> Erro: " + error.message)
         document.getElementById('recaptcha-security').innerHTML = "Not authenticaded!"
      })
}