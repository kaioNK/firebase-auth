
const logIn = (email, password, email_id, password_id) => {
   console.log("LogIn - email: " + email +", password: " + password)
   firebase.auth().setPersistence(firebase.auth.Auth.Persistence.NONE)
      .then(() => {
         firebase.auth().signInWithEmailAndPassword(email, password)
            .then((user) => {
               console.log('Usuário fez login!')
               document.getElementById(email_id).value = ''
               document.getElementById(password_id).value = ''
            })
            .catch((error) => {
               console.error('Usuário não fez login!')
            })
      })
      .catch((error) => {
         console.error("Método: logIn ==> Erro: " + error.message)
      })
   
}

const register = (email, password, email_id, password_id) => {
   console.log("Register - email: " + email + ', password: ' + password)
   firebase.auth().createUserWithEmailAndPassword(email, password)
      .then((user) => {
         console.log("Usuário criado com sucesso!")
         document.getElementById(email_id).value = ''
         document.getElementById(password_id).value = ''
      })
      .catch((error) => {
         console.error("Usuário não pode ser criado!")
      })
}

const getProfileData = () => {
   const user = firebase.auth().currentUser
   if(user != null){
      console.log("USER = ", user)
   }else{
      console.error("Não existe usuário logado para obtermos seus dados!")
   }
}

const getUser = () => {
   return firebase.auth().currentUser
}

const logout = () => {
   const user = getUser()

   if(user != null){
      firebase.auth().signOut()
         .then(() => {
            console.log("Usuário fez logout com sucesso!")
         })
         .catch((error) => {
            console.error("Erro ao fazer logout: " + error.message)
         })
   }else{
      console.error("Não existe usuário logado!")
   }
}

//Lembre-se de clicar no botão Get Profile para exibir os dados no console
const updateProfileUser = (displayName, emailUpdated) => {
   const user = getUser()

   if(user != null){
      user.updateProfile({
         //propriedades: valor
         displayName: displayName
      })

      user.updateEmail(emailUpdated)
         .then(() => {
            console.log("Email alterado com sucesso!")
         })
         .catch((error) => {
            console.error("Erro ao alterar o email! => " + error.message)
      })

   }else{
      console.error("Não existe usuário logado para fazermos update nos seus dados!")
   }
}

const verifyUser = () => {
   const user = getUser()
   user.sendEmailVerification()
      .then(() => {
         console.log("Email enviado")
      })
      .catch((error) => {
         console.error("Erro ao enviar o email de verificação. Erro: " + error.message)
      })
}

const resetPassword = () => {
   const user = getUser()
   firebase.auth().sendPasswordResetEmail(user.email)
      .then(() => {
         console.log("Email para resetar password enviado!")
      })
      .catch((error) => {
         console.error("Erro ao enviar o email para resetar o password. Erro: " + error.message)
      })
}

const deleteUser = () => {
   const user = getUser()
   user.delete()
      .then(() => {
         console.log("Usuário deletado com sucesso!")
      })
      .catch((error) => {
         console.error("Erro ao deletar o usuário. Erro: " + error.message)
      })
}